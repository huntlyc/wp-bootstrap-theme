
WP Bootstrap Theme
==================

Simple skeleton [Wordpress](http://wordpress.org/ "Wordpress") theme that has nothing in it other than the bare minimum to get going.

Why?
----
Because the amount of times I've downloaded [twitter bootstrap](http://twitter.github.com/bootstrap/ "Twitter Bootstrap") and had to hack away at it to build a theme to test out something is not even funny. Sure, you're right I _could_ use the twenty ```echo date('y');``` theme, but I didn't want all the extra functionality that comes with those themes. 

What's included?
----------------
Not a lot. Basically you get the minimum required for a Wordpress theme:

* **index.php** - Main loop sits in here
* **style.css** - Some slight overrides to the bootstrap to get it working
* **header.php** - Navigation sits in here
* **footer.php** - Copyright stuff

What's **not** included?
------------------------
* post listings
* comments
* the meaning of life (you're on your own there)
