<?php
    //http://stackoverflow.com/questions/24942369/change-class-name-in-wordpress-wp-list-page-nav/24970491#24970491
    class Custom_Nav_Walker extends Walker_Page {

        /**
         * @see Walker::start_lvl()
         * @since 2.1.0
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param int $depth Depth of page. Used for padding.
         * @param array $args
         */
        function start_lvl( &$output, $depth = 0, $args = array() ) {
            $indent = str_repeat("\t", $depth);
            $output .= "\n$indent<ul class='dropdown-menu children'>\n";
        }

        /**
         * @see Walker::start_el()
         * @since 2.1.0
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param object $page Page data object.
         * @param int $depth Depth of page. Used for padding.
         * @param int $current_page Page ID.
         * @param array $args
         */
        function start_el( &$output, $page, $depth = 0, $args = array(), $current_page = 0 ) {
            if ( $depth )
                $indent = str_repeat("\t", $depth);
            else
                $indent = '';

            extract($args, EXTR_SKIP);
            $css_class = array('page_item', 'page-item-'.$page->ID);

            if( isset( $args['pages_with_children'][ $page->ID ] ) )
                $css_class[] = 'page_item_has_children dropdown';

            if ( !empty($current_page) ) {
                $_current_page = get_post( $current_page );
                if ( in_array( $page->ID, $_current_page->ancestors ) )
                    $css_class[] = 'current_page_ancestor';
                if ( $page->ID == $current_page )
                    $css_class[] = 'current_page_item';
                elseif ( $_current_page && $page->ID == $_current_page->post_parent )
                    $css_class[] = 'current_page_parent';
            } elseif ( $page->ID == get_option('page_for_posts') ) {
                $css_class[] = 'current_page_parent';
            }

            /**
             * Filter the list of CSS classes to include with each page item in the list.
             *
             * @since 2.8.0
             *
             * @see wp_list_pages()
             *
             * @param array   $css_class    An array of CSS classes to be applied
             *                             to each list item.
             * @param WP_Post $page         Page data object.
             * @param int     $depth        Depth of page, used for padding.
             * @param array   $args         An array of arguments.
             * @param int     $current_page ID of the current page.
             */
            $css_class = implode( ' ', apply_filters( 'page_css_class', $css_class, $page, $depth, $args, $current_page ) );

            if ( '' === $page->post_title )
                $page->post_title = sprintf( __( '#%d (no title)' ), $page->ID );

            /** This filter is documented in wp-includes/post-template.php */
            if(preg_match('/dropdown/', $css_class) != FALSE){
                $output .= $indent . '<li class="' . $css_class . '">
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="' . get_permalink($page->ID) . '">' .
                                          $link_before . apply_filters( 'the_title', $page->post_title, $page->ID ) . $link_after .
                                        '</a>';
            }else{
                $output .= $indent . '<li class="' . $css_class . '">
                                        <a href="' . get_permalink($page->ID) . '">' .
                                          $link_before . apply_filters( 'the_title', $page->post_title, $page->ID ) . $link_after .
                                        '</a>';
            }

            if ( !empty($show_date) ) {
                if ( 'modified' == $show_date )
                    $time = $page->post_modified;
                else
                    $time = $page->post_date;

                $output .= " " . mysql2date($date_format, $time);
            }
        }
    }