<div class="masthead">
    <h3 class="muted"><?php bloginfo('title'); ?>&nbsp;<small><?php bloginfo('description'); ?></small></h3>
	<div class="navbar">
  	<div class="navbar-inner">
    	<div class="container">
      	<ul class="nav">
        	<?php
        		wp_list_pages( array( 'depth' => 0,
        	                          'sort_column' => 'menu_order',
        	                          'title_li' => '' )
				);
		    ?>
      	</ul>
    	</div>
  	</div>
	</div><!-- /.navbar -->
</div>
