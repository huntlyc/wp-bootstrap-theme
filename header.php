<div class="masthead">
    <h3 class="muted"><?php bloginfo('title'); ?>&nbsp;<small><?php bloginfo('description'); ?></small></h3>
</div>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <!-- Everything you want hidden at 940px or less, place within here -->
            <div class="nav-collapse collapse">
                <!-- .nav, .navbar-search, .navbar-form, etc -->
                <ul class="nav">
                    <?php
                         wp_list_pages(
                            array(
                                'depth' => 2,
                                'sort_column' => 'menu_order',
                                'title_li' => '',
                                'walker' =>  new Custom_Nav_Walker()
                            )
                        );
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>

