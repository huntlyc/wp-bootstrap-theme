
<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
    	<title><?php bloginfo('title'); ?></title>
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta name="description" content="">
    	<meta name="author" content="You">
	    <?php
	    	//Enqueue all the styles
		    wp_enqueue_style( $handle = 'bootstrap-main',
		    	              $src = get_bloginfo('template_url') . '/css/bootstrap.css');
		    wp_enqueue_style( $handle = 'bootstrap-responsive',
		    	              $src = get_bloginfo('template_url') . '/css/bootstrap-responsive.css');
		    wp_enqueue_style( $handle = 'main-theme-style',
		    				   $src = get_bloginfo('template_url') . '/style.css');
		    //Load up bootrap js in the footer, requiring jQuery
		    wp_enqueue_script( $handle = 'bootstrap-js',
		    	               $src = get_bloginfo('template_url') . '/js/bootstrap.js',
		    	               $deps = array('jquery'),
		    	               $ver = false,
		    	               $loadInFooter = true);

            wp_enqueue_script( $handle = 'twnav',
                               $src = get_bloginfo('template_url') . '/js/twnav.js',
                               $deps = array('jquery'),
                               $ver = false,
                               $loadInFooter = true);
	   	?>
	    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	    <!--[if lt IE 9]>
	      <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
	    <![endif]-->
	    <?php wp_head(); ?>
	</head>
	<body>
    	<div class="container">
	      	<?php get_header(); ?>
	      	<div class="row-fluid">
		        <div class="span12">
		        	<?php if(have_posts()): ?>
		        		<?php while(have_posts()): the_post(); ?>
		        			<?php the_content(); ?>
		        		<?php endwhile; ?>
		        	<?php endif; ?>
	        	</div>
	      	</div>
	      	<hr>
	      	<?php get_footer(); ?>
	      	<?php wp_footer(); ?>
	    </div> <!-- /container -->
  	</body>
</html>
