//override for close
jQuery('a.dropdown-toggle').click(function(e){
    e.preventDefault();
    var link = jQuery(this);
    var parantli = link.parent('li');
    //IF its closing, link straight to the page
    if(parantli.hasClass('page_item_has_children') && parantli.hasClass('open')){

        var linkURL = link.attr('href');
        window.location = linkURL;
    }
})